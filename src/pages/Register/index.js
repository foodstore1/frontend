import * as React from 'react'; 
import { LayoutOne, Card,  FormControl, InputText, InputPassword, Button } from 'upkit';
import { useForm } from 'react-hook-form';
import { rules } from './validation';
import { registerUser } from '../../api/auth';
import { useNavigate, Link } from 'react-router-dom';
import StoreLogo from '../../components/StoreLogo';

// (1) statuslist
const statuslist = {
    idle: 'idle', 
    process: 'process', 
    success: 'success', 
    error: 'error', 
}

export default function Register(){
    let { register, handleSubmit, formState: {errors}, setError } = useForm();
    // fungsi register yang berasal dari useForm hanya kebetulan saja bernama sesuai komponen saat
    // ini yaitu Register, tidak ada hubungannya sama sekali ya.

    // state status dengan nilai default `statuslist.idle`
    let [ status, setStatus ] = React.useState(statuslist.idle);
    let history = useNavigate();

    const onSubmit = async formData => {
        // (1) dapatkan variabel password dan password_confirmation
        let { password, password_confirmation } = formData; 
        // (2) cek password vs password_confirmation 
        if(password !== password_confirmation) {
            return setError('password_confirmation', {type: 'equality',
            message: 'Konfirmasi password harus dama dengan password'});
        }

        setStatus(statuslist.process);

        let {data} = await registerUser(formData)

        // (1) cek apakah ada error
        if(data.error){
        
            // (2) dapatkan field terkait jika ada errors
            let fields = Object.keys(data.fields);
            // (3) untuk masing-masing field kita terapkan error dan tangkap pesan errornya
            fields.forEach(field => {
                setError(field, {type: 'server', message: data.fields[field]?.properties?.message})
            });

            setStatus(statuslist.error);
        }

        setStatus(statuslist.success);

        history('/register/berhasil');
    }

    return (
    <LayoutOne size="small">
        <Card color="white" >
        <div className="text-center mb-5"> 
            <StoreLogo/> 
        </div>
            <form onSubmit={handleSubmit(onSubmit)}>
            <FormControl errorMessage={errors?.full_name?.message}>
                <InputText
                    name="full_name"
                    placeholder="Nama Lengkap"
                    fitContainer
                    {...register('full_name', rules.full_name)}
                    // prop fitContainer digunakan agar lebar dari
                    // InputText mengikuti lebar dari komponen parent
                />
            </FormControl>
            <FormControl errorMessage={errors?.email?.message}>
                <InputText 
                    name="email"
                    placeholder="Email"
                    fitContainer
                    {...register('email', rules.email)}
                />
            </FormControl>
            <FormControl errorMessage={errors?.password?.message}>
                <InputPassword
                    name="password"
                    placeholder="Password"
                    fitContainer
                    {...register('password', rules.password)}
                />
            </FormControl>
            <FormControl errorMessage={errors?.password_confirmation?.message}>
                <InputPassword 
                    name="password_confirmation"
                    placeholder="Konfirmasi Password"
                    fitContainer
                    {...register('password_confirmation',rules.password_confirmation)}
                />
            </FormControl>
            <Button
                size="large"
                fitContainer
                disabled={status === statuslist.process}
            >  {status === statuslist.process ? "Sedang memproses" : "Mendaftar"} 
            </Button>
            </form>
            <div className="text-center mt-2">
                Sudah punya akun? 
                <Link to="/login"> <b> Masuk Sekarang. </b> </Link> 
            </div>
        </Card>
    </LayoutOne>
    )
}