import { HashRouter as Router, Route, Routes } from 'react-router-dom';
import 'upkit/dist/style.min.css'
import { Provider } from 'react-redux';
import store from './app/store';
import { listen } from './app/listener';
import React from 'react';

import Home from './pages/Home';
import Register from './pages/Register';
import RegisterSuccess from './pages/RegisterSuccess';
import Login from './pages/Login';

function App() {
  React.useEffect(() => {
    listen();
  },[])
//   Kamu mungkin bertanya, kenapa kita menggunakan dependency Array kosong [] padahal kita
// menggunakan fungsi listen di dalam Hook useEffect. Bukankah ini menyalahi prinsip
// exhaustive-deps? Jawabanya TIDAK, karena fungsi listen didefinisikan di luar komponen sehingga
// identity nya akan selalu dianggap sama sehingga tidak ada masalah mau kita masukkan atau tidak
// sebagai deps.
// Dengan demikian maka sekarang listener kita akan dijalankan setiap kali ada perubahan state.

  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Routes>
            <Route path="/register/berhasil" element={<RegisterSuccess/>}/>
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/" element={<Home/>}/>
          </Routes>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
