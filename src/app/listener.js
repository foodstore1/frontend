import store from './store';

// Kenapa kita perlu membuat variabel currentAuth, karena kita ingin membandingkan apakah nilai dari
// Auth berubah atau tidak. Jika berubah baru kita lakukan operasi penyimpanan ke Local Storage, jangan
// sampai ada perubahan Redux state tapi bukan berasal dari Auth kita tetep melakukan penyimpanan ke
// Local Storage.
let currentAuth;

function listener(){
    // (1) buat variabel previousAuth dan berikan currentAuth sebagai nilai
    let previousAuth = currentAuth;
    // (2) update nilai currentAuth dari nilai state terbaru 
    currentAuth = store.getState().auth;
    // (3) cek apakah nilai state `auth` berubah dari nilai sebelumnya 
    if(currentAuth !== previousAuth){
        // (4) jika berubah simpan ke localStorage 
        localStorage.setItem('auth', JSON.stringify(currentAuth));
    }
}

// (1) buat fungsi listen
function listen(){
    // (1) dengarkan perubahan store, Kita gunakan fungsi store.subscribe untuk mendaftarkan fungsi listener, fungsi listener ini
    // akan subscribe atau mendengarkan perubahan Redux store dan akan dipanggil setiap ada perubahan
    // tertentu.
    store.subscribe(listener);
}

// (2) export fungsi listen supaya bisa digunakan di file lain 
export { listen }